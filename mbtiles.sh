#!/bin/sh

ogr2ogr -f GeoJSONSeq -dialect SQLITE -sql "SELECT CAST(strftime('%s', datetime(datetime)) AS integer) AS date, geometry FROM hotspots WHERE sensor != 'AVHRR'" /vsistdout -oo X_POSSIBLE_NAMES=longitude -oo Y_POSSIBLE_NAMES=latitude hotspots.csv | \
    tippecanoe --force --output hotspots.mbtiles -l 'hotspots' -r0 --drop-densest-as-needed
