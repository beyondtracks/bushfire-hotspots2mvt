#!/bin/sh

if [ ! -e hotspots.csv ] ; then
    echo "Initial full download..."
    wget -O hotspots.csv 'https://hotspots.dea.ga.gov.au/geoserver/wfs?service=wfs&version=1.1.1&request=getfeature&outputFormat=csv&typenames=public:hotspots'
else
    echo "Incremental update..."
    highestID=`cut -d',' -f2 hotspots.csv | sort -nr | head -n1`
    linesBefore=`wc -l hotspots.csv | cut -d' ' -f1`
    wget -O - "https://hotspots.dea.ga.gov.au/geoserver/wfs?service=wfs&version=1.1.1&request=getfeature&outputFormat=csv&typenames=public:hotspots&cql_filter=id>${highestID}" | tail -n+2 >> hotspots.csv
    linesAfter=`wc -l hotspots.csv | cut -d' ' -f1`
    echo "$(($linesAfter - $linesBefore)) new features"
fi 
